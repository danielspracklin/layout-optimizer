# layout-optimizer

R code to optimize the layout of various keyboards according to specified rulesets

The intent of this project is to design an optimal keyboard layout _conditional on_ the text I'm most likely to write:

- programming in R
- occasionally typing in French
